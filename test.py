import time
import math
import json
import os
import argparse

from PIL import Image, ImageDraw

class Constant:
    #Необходимые константы
    HOME_WIDTH = 350
    HOME_EDGE = 196
    HOME_HEIGHT = int((HOME_EDGE ** 2 - (HOME_WIDTH / 2) ** 2) ** 0.5) * 2
    BLOCK_WIDTH = HOME_WIDTH * 4
    BLOCK_HEIGHT = HOME_HEIGHT * 4
    BORDER_WIDTH = HOME_WIDTH / 2
    BORDER_HEIGHT = HOME_HEIGHT / 2
    VERT_DELT = (96**2 - (HOME_WIDTH / 4)**2) ** 0.5
    HOMES = 12

class Map:
    def __init__(self, home_amount):
        self.__argument_check(home_amount)
        self.home_amount = home_amount
        self.block_amount = math.ceil(home_amount / Constant.HOMES)
        self.blocks_info = self.get_blocks_info() 
        self.picture_size = self.get_picture_size()
        self.coordinates = self.calc_coordinates()

    def draw(self, file_name: str, path: str):
        #Отрисовка изображения
        transparent = (255, 255, 255, 0)
        half_transparent = (0, 255, 0, 127)

        size = self.picture_size
        img = Image.new('RGBA', size, transparent)
        drw = ImageDraw.Draw(img)

        for pos in self.coordinates:
            drw.polygon(self.get_draw_coordinates(pos), fill=half_transparent, outline=transparent)

        img.save(os.path.join(path, file_name + '.png'), 'PNG')

    def save_to_json(self, file_name, path):
        #Сохраняем в JSON
        width, height = self.picture_size
        result = {
            'width': width,
            'height': height,
            'coordinates': self.coordinates
        }
        with open(os.path.join(path, file_name + '.json'), 'w') as f:
            json.dump(result, f)

    def get_blocks_info(self):
        #Получаем кол-во строк, колонок
        rows = int(self.block_amount ** 0.5)
        columns = int(math.ceil(self.block_amount / rows))
        summ = int(rows + columns)
        rows = int(summ*2/3)
        columns = int(summ-rows)
        remainder = self.block_amount - (rows*columns)
        if remainder > 0:
            rows += 1
        
        return {'rows': rows, 'columns': columns}
    
    def get_start_coord(self, init_x, init_y, home_in_block, parity):
        #Считаем стартовые координаты домиков в блоке
        result = []
        tmp = -1 if parity else 1
        
        for i in range (int(home_in_block / 4)):
            x = int(init_x + i * (Constant.HOME_WIDTH * 3 / 4))
            y = int(init_y + tmp * i * (Constant.HOME_HEIGHT / 2 + Constant.VERT_DELT))
            for j in range (4):
                result +=[[int(x + j * Constant.HOME_WIDTH / 2),
                int(y - tmp * j * Constant.HOME_HEIGHT / 2)]]
        if home_in_block < Constant.HOMES:
            i = int(home_in_block / 4)
            x = int(init_x + i * (Constant.HOME_WIDTH * 3 / 4))
            y = int(init_y + tmp * i * (Constant.HOME_HEIGHT / 2 + Constant.VERT_DELT)) 
            for j in range (int (home_in_block % 4)):
                result +=[[int(x + j * Constant.HOME_WIDTH / 2),
                int(y - tmp * j * Constant.HOME_HEIGHT / 2)]]

        return result

    def block_pos(self, row, col):
        #Метод расчёта координат опредёлнного блока
        x = 0
        y = Constant.BLOCK_HEIGHT / 2
        #Если чётный ряд, то сдвигаем его правее
        if not row % 2 and self.blocks_info['rows'] > 1:
            x += (Constant.BLOCK_WIDTH + Constant.BORDER_WIDTH) / 2
        # Т.к. Строк в 2 раза больше колонк => и вниз надо идти по половине длины и границы
        x += (Constant.BLOCK_WIDTH + Constant.BORDER_WIDTH) * col
        y += (Constant.BLOCK_HEIGHT + Constant.BORDER_HEIGHT) / 2 * row

        return int(x), int(y)

    def calc_coordinates(self):
    #Основной метод расчёта координат
        stop = False
        result = []
        amount = self.home_amount
        for i in range(self.blocks_info['rows']):
            for j in range(self.blocks_info['columns']):
                home_in_block = Constant.HOMES if amount > Constant.HOMES else amount
                amount -= Constant.HOMES
                x, y = self.block_pos(i, j)
                result += self.get_start_coord(x, y, home_in_block, i % 2)
                if (amount < 0):
                    stop = True
                    break
            if (stop):
                break
        return result

    def get_picture_size(self):
        #Метод расчёта размера изображения
        height = self.blocks_info['rows'] * (Constant.BLOCK_HEIGHT + Constant.BORDER_HEIGHT) / 2 + Constant.BLOCK_HEIGHT / 2
        width = self.blocks_info['columns'] * (Constant.BLOCK_WIDTH + Constant.BORDER_WIDTH)
        width += (Constant.BLOCK_WIDTH + Constant.BORDER_WIDTH) / 2

        return math.ceil(width), math.ceil(height)

    def get_draw_coordinates(self, start):
        #Метод расчёта координат каждых 4 углов домика
        return [tuple (start),
                (start[0] + Constant.HOME_WIDTH / 2, start[1] - Constant.HOME_HEIGHT / 2),
                (start[0] + Constant.HOME_WIDTH, start[1]),
                (start[0] + Constant.HOME_WIDTH / 2, start[1] + Constant.HOME_HEIGHT / 2)]

    def __argument_check(self, amount):
        if type(amount) is not int or amount < 1:
            raise ValueError('Необходимо натуральное число.')

if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('amount', type=int, help='Кол-во объектов')

    args = parser.parse_args()

    dir_exists = os.path.isdir('out1')
    if not dir_exists:
        os.makedirs(args.dir)
        dir_exists = True

    if dir_exists:
        start_time = time.time()
        m = Map(args.amount)
        m.save_to_json('data', 'out1')
        print("Время выполнения (JSON) = %s секунд" % (time.time() - start_time))
        start_time = time.time()
        m.draw('image', 'out1')
        print("Время выполнения (Image) = %s секунд" % (time.time() - start_time))
